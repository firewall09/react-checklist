import React from 'react';
import ReactModal from 'react-modal';

class App extends React.Component {
  constructor (props){
    super(props);

  }

  render (){
    return (
      <div>
        <AppJumbotron title="React Checklist" />
        <FormToDo />

        <ItemList />
        <br />
        <br />
        <br />
        <ItemCount count={allTheThings.length} />
        <hr />
        <AppFooter />
      </div>
    );
  }
}

class FormToDo extends React.Component {
  constructor (props){
    super(props);
    if(props.state) {
      console.log(props.state);
      this.state = props.state;
    }
    else {
      this.state =  {
        id: '',
        title: '',
        description: '',
        date: ''
      };
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {   
    localStorage.setItem(this.state.id, JSON.stringify(this.state));
    location.reload();
    event.preventDefault();
  }

  render() {
    var idLabel= '';
      if(this.state.id == ''){
        idLabel = <label>
          ID:
          <input 
            name="id" 
            type="number" 
            value={this.state.id} 
            onChange={this.handleChange} />
        </label>;
      }
    return (
      <form onSubmit={this.handleSubmit}>
        {idLabel}
        <br />
        <label>
          Title:
          <input
            name="title"
            type="text"
            value={this.state.title}
            onChange={this.handleChange} />
        </label>
        <br />
        <label>
          Description:
          <input
            name="description"
            type="text"
            value={this.state.description}
            onChange={this.handleChange} />
        </label>
        <br />
        <label>
          Date:
          <input
            name="date"
            type="date"
            value={this.state.date}
            onChange={this.handleChange} />
        </label>
        <br />
        <input type="submit" value="Submit" />
      </form>
    );
  }
}


class Item extends React.Component {
  constructor (props){
    super ();

    this.state = {
      checked: false,
      showModal: false
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  handleClick (e){
    this.setState({
      checked: !this.state.checked
    });

  }

  handleRemove (id) {
    localStorage.removeItem(id);
    location.reload();
  }

  handleOpenModal () {
    this.setState({ showModal: true });
  }
  
  handleCloseModal () {
    this.setState({ showModal: false });
  }
  

  render (){
  	var todoText = this.props.title;
    var text = this.state.checked ? <strike>{todoText}</strike> : todoText;
    return (
        <div className="row">
          <div className="col-md-12">
            <input type="checkbox" onClick={this.handleClick} />
            &nbsp;{text}
            <button type="button" onClick={() => this.handleRemove(this.props.id)}>Remove</button>
            <button onClick={this.handleOpenModal}>View Detail</button>
            <ReactModal 
               isOpen={this.state.showModal}
               contentLabel="Todo List Modal"
            >
              <div>
                <h1>Detail todo list :{this.props.title}</h1>
                <div>ID: {this.props.id}</div>
                <div>Description: {this.props.description}</div>
                <div>Date: {this.props.date}</div>
              </div>
              <div>
                <h1>Update todo list :{this.props.title}</h1>
                <FormToDo state={this.props} />
              </div>
              <button onClick={this.handleCloseModal}>Close</button>
            </ReactModal>
          </div>
        </div>
    );
  }
}

function allStorage() {

    var values = [],
        keys = Object.keys(localStorage),
        i = keys.length;

    while ( i-- ) {
    	var temp = localStorage.getItem(keys[i]);
      values.push(temp);
    }

    return values;
}

let allTheThings = allStorage();

class ItemList extends React.Component {
  constructor (props){
    super ();
  }

  render (){
    var todoList = allTheThings.map(function(item){
      var objectTodo = JSON.parse(item);
      var todo = <Item
        key={objectTodo.id} 
        id={objectTodo.id} 
        title={objectTodo.title} 
        description={objectTodo.description}
        date={objectTodo.date}
        />;
      return todo;
    });


    // let items = allTheThings.map(thing => thing);
    // console.log(items);
    return (
        <h4>{todoList}</h4>
    );
  }
}

class ItemCount extends React.Component {
  constructor (props){
    super ();
  }
  render (){
    return (
      <h4>There are {this.props.count} items on your list</h4>
    );
  }
}

class AppJumbotron extends React.Component {
  render (){
    return (
      <div className="jumbotron">
        <h2>{this.props.title}</h2>
      </div>
    );
  }
}

class AppFooter extends React.Component {
  render (){
    return (
      <div className="text-muted">
        <small>&copy; {new Date().getFullYear()}</small>
      </div>
    );
  }
}

export default App;